## PROJETO DE TESTE COM MQTT, SOCKET.IO E WEBSERVER

# IMPORTANTE

Para rodar, vá no arquivo "mqtt.js" e altere o valor da variável para o IP local da máquina que possui o broker MQTT.

Não esqueça de rodar o comando **npm install** para instalar as dependências.

Para iniciar, rode o comando **npm start**.

# REFERÊNCIAS

- [Como criar um broker MQTT no Linux](https://www.hackster.io/dhairya-parikh/running-a-mqtt-broker-on-raspberry-pi-63c348)
- [Download NodeJS no Linux](https://tecadmin.net/install-latest-nodejs-npm-on-ubuntu/)
