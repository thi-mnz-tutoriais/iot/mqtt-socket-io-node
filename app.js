const express = require('express'); //Biblioteca para construir servidores web
const app = express();
const http = require('http');
const server = http.createServer(app); //Cria o servidor de fato
const { Server } = require("socket.io");
const io = new Server(server); //Cria o webSocket
const initializeMQTT = require("./mqtt") //Importa a função que foi criada no arquivo mqtt.js

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html'); //Arquivo estático que é renderizado quando URL é /
});

io.on('connection', (socket) => { // Evento padrão do socket.io quando alguém se conecta.
  console.log('Alguém se conectou!');
});

server.listen(3000, () => { //Função que inicia o servidor web e o socket.
  initializeMQTT(io)
  console.log('listening on *:3000');
});