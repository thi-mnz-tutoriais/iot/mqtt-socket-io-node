var mqtt = require('mqtt') //Biblioteca cliente do MQTT
var ip = "0.0.0.0" //Digite o IP da máquina que contém o broker MQTT
const PORT = 1883
var client  = mqtt.connect('mqtt://' + ip + ':' + PORT) //Conexão ao broker
const fs = require("fs") //Biblioteca de acesso de arquivos do sistema

//Função que inicia a comunicação e dispara callbacks
const initializeMQTT = (io) => {
  client.on('connect', function () {
    client.subscribe('test/message', function (err) {
      console.log("subscribed successfully")
    })
  })
  
  client.on('message', function (topic, message) {
    //Salva no log e emite evento para o socket
    fs.appendFileSync("output.log", message.toString() + '\n')
    io.emit("temperatura", message.toString())
  })
}

//Exporta a(s) função(ões) para o arquivo que as importar
module.exports = initializeMQTT